---
layout: markdown_page
title: "Legal"
---

## On this page
{:.no_toc}

- TOC
{:toc}
 
## Communication
 
### Specific Legal Services and Advice

If you need to request legal services, such as third party license review or legal advice/guidance, please check the [legal processes section](/handbook/legal/#legal-team-processes) of this page to see if your need is already addressed. If not, submit your request by sending an [email](mailto:legal@gitlab.com). This will send your request through the Legal private issue tracker. Through the Legal private issue tracker, you will be updated regarding the status of your request through the Service Desk feature.
 
Please be sure to include sufficient detail regarding your request, including time-sensitive deadlines, relevant documents, and background information necessary to respond. 

The Executive Team will have full access to the [Legal issue tracker](https://gitlab.com/gitlab-legal/legal-issue-tracker).

### General Legal Questions

Feel free to use the `#legal` chat channel in Slack for general legal questions that don't seem appropriate for the issue tracker or internal email correspondence.  Slack is not to be used for anything that is considered confidential or seeking legal advice.

### Sales Contracts

If a customer requests to edit our standard Subscription or Services Agreements, please follow the process outlined in the [Sales Handbook](https://about.gitlab.com/handbook/sales/#process-for-agreement-terms-negotiations-when-applicable).


## Contract Templates
 
* [Employment and Contractor Agreements](/handbook/contracts/)
* Non-Disclosure Agreement (NDA) (WIP)
 
## Legal Team Processes
 
* [Signing Legal Documents](/handbook/signing-legal-documents/)
* [Authorization Matrix](/handbook/finance/authorization-matrix/)
* [Approval for Vendor Contracts](/handbook/finance/procure-to-pay)
* Negotiating Contracts with Customers [Sales Roles](/handbook/sales/#process-for-agreement-terms-negotiations-when-applicable); [Legal Role] (/handbook/legal/customer-negotiations/index.html)
 
## Company Policies
 
[Anti-harassment](/handbook/anti-harassment/)
 
 
## General Topics

Frequently Asked Questions (WIP)
 
 
## Other Pages Related to Legal
 
* [Company Information](https://gitlab.com/gitlab-com/finance/wikis/company-information)
* [General Guidelines](/handbook/general-guidelines/)
* [Terms](/terms/)
* [Privacy Policy](/privacy/)